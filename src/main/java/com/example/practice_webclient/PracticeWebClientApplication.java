package com.example.practice_webclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeWebClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PracticeWebClientApplication.class, args);
    }

}

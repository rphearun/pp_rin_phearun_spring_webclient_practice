package com.example.model.dto;

import com.example.model.DTO.CourseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto {
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private LocalDateTime birthDate;
    private CourseDTO courseDto;


}

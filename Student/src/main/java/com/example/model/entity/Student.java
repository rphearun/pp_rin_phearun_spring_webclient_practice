package com.example.model.entity;


import com.example.model.DTO.CourseDTO;
import com.example.model.dto.StudentDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstname;

    private String lastname;

    private String email;

    @Temporal(TemporalType.TIMESTAMP)
    private LocalDateTime birthDate;

    private Long courseId;


    public StudentDto toDto(CourseDTO courseDto){
        return new StudentDto(this.id,this.firstname,this.lastname,this.email,this.birthDate,courseDto);
    }


}

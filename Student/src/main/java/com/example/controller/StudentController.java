package com.example.controller;

import com.example.model.DTO.CourseDTO;
import com.example.model.dto.StudentDto;
import com.example.model.request.StudentRequest;
import com.example.model.response.ApiResponse;
import com.example.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }
    @GetMapping("/")
    public ResponseEntity<?> getAllCourse(){
        ApiResponse<List<StudentDto>> response = ApiResponse.<List<StudentDto>>builder()
                .message("successfully fetch data")
                .status("200")
                .payload(studentService.getAllCourse())
                .build();
        return ResponseEntity.ok(response);
    }
    @PostMapping("/add")
    public ResponseEntity<?> addStudent(@RequestBody StudentRequest studentRequest){
        ApiResponse<StudentDto> response = ApiResponse.<StudentDto>builder()
                .message("successfully add data")
                .status("200")
                .payload(studentService.addStudent(studentRequest))
                .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getStudentById(@PathVariable Long id){
        ApiResponse<StudentDto> response = ApiResponse.<StudentDto>builder()
                .message("successfully add data")
                .status("200")
                .payload(studentService.getStudentById(id))
                .build();
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable Long id , @RequestBody StudentRequest studentRequest ){
        ApiResponse<StudentDto> response = ApiResponse.<StudentDto>builder()
                .message("successfully add data")
                .status("200")
                .payload(studentService.updateStudentById(id,studentRequest))
                .build();
        return ResponseEntity.ok(response);

    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable Long id ){
        var delete = studentService.deleteStudentById(id);
        ApiResponse<StudentDto> response = ApiResponse.<StudentDto>builder()
                .message("successfully delete data")
                .status("200")
                .build();
        return ResponseEntity.ok(response);


    }



}

package com.example.service;

import com.example.model.DTO.CourseDTO;
import com.example.model.dto.StudentDto;
import com.example.model.entity.Student;
import com.example.model.request.StudentRequest;
import com.example.model.response.ApiResponse;
import com.example.repository.StudentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final WebClient webClient;


    public StudentServiceImpl(StudentRepository studentRepository, WebClient.Builder webClientBuilder) {
        this.studentRepository = studentRepository;
        this.webClient = webClientBuilder.baseUrl("http://localhost:8181/api/v1/course/").exchangeStrategies(ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder()))
                .build()).build();
    }

    @Override
    public List<StudentDto> getAllCourse() {
        List<Student> students = studentRepository.findAll();

        return students.stream()
                .map(student -> {
                    ApiResponse<CourseDTO> apiResponse = webClient
                            .get()
                            .uri("/{id}", student.getCourseId())
                            .retrieve()
                            .bodyToMono(new ParameterizedTypeReference<ApiResponse<CourseDTO>>() {})
                            .block();

                        CourseDTO courseDto = apiResponse.getPayload();
                        return new StudentDto(
                                student.getId(),
                                student.getFirstname(),
                                student.getLastname(),
                                student.getEmail(),
                                student.getBirthDate(),
                                courseDto
                        );
                })
                .collect(Collectors.toList());
    }


    @Override
    public StudentDto addStudent(StudentRequest studentRequest) {
        Long courseId = studentRequest.getCourseId();

        ApiResponse<CourseDTO> apiResponse = webClient
                .get()
                .uri("/{id}", courseId)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<ApiResponse<CourseDTO>>() {})
                .block();

            CourseDTO courseDto = apiResponse.getPayload();
            Student student = new Student(
                    null,
                    studentRequest.getFirstName(),
                    studentRequest.getLastName(),
                    studentRequest.getEmail(),
                    studentRequest.getBirthDate(),
                    studentRequest.getCourseId()
            );

            Student savedStudent = studentRepository.save(student);
            return savedStudent.toDto(courseDto);

    }

    @Override
    public StudentDto getStudentById(Long id) {
        Optional<Student> studentOptional = studentRepository.findById(id);
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();
            Long courseId = student.getCourseId();

            ApiResponse<CourseDTO> apiResponse = webClient
                    .get()
                    .uri("/{id}", courseId)
                    .retrieve()
                    .bodyToMono(new ParameterizedTypeReference<ApiResponse<CourseDTO>>() {})
                    .block();

            CourseDTO courseDto = apiResponse.getPayload();
            return new StudentDto(
                    student.getId(),
                    student.getFirstname(),
                    student.getLastname(),
                    student.getEmail(),
                    student.getBirthDate(),
                    courseDto
            );
        } else {
            throw new RuntimeException("Student not found with ID: " + id);
        }
    }

    @Override
    public StudentDto updateStudentById(Long id, StudentRequest studentRequest) {
        Student student = studentRepository.findById(id).get();
        Long courseId = studentRequest.getCourseId();
        ApiResponse<CourseDTO> apiResponse = webClient
                .get()
                .uri("/{id}", courseId)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<ApiResponse<CourseDTO>>() {})
                .block();

            CourseDTO courseDto = apiResponse.getPayload();

            student.setFirstname(studentRequest.getFirstName());
            student.setLastname(studentRequest.getLastName());
            student.setEmail(studentRequest.getEmail());
            student.setBirthDate(studentRequest.getBirthDate());
            student.setCourseId(studentRequest.getCourseId());

            Student updatedStudent = studentRepository.save(student);

            return updatedStudent.toDto(courseDto);

    }

    @Override
    public String deleteStudentById(Long id) {
        studentRepository.deleteById(id);
        return "successfuly delete student";
    }
}


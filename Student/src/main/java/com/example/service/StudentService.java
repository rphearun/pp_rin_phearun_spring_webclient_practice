package com.example.service;

import com.example.model.dto.StudentDto;
import com.example.model.request.StudentRequest;

import java.util.List;

public interface StudentService {
    List<StudentDto> getAllCourse();

    StudentDto addStudent(StudentRequest studentRequest);

    StudentDto getStudentById(Long id);

    StudentDto updateStudentById(Long id, StudentRequest studentRequest);

    String deleteStudentById(Long id);
}

package com.example.service;

import com.example.model.DTO.CourseDTO;
import com.example.model.entity.Course;
import com.example.model.request.CourseRequest;
import com.example.repository.CourseRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<CourseDTO> getAllCourse() {
        return courseRepository.findAll().stream().map(Course::toDto).toList();
    }

    @Override
    public CourseDTO addCourse(CourseRequest courseRequest) {
        var courseEntity =courseRequest.toEntity();
        return courseRepository.save(courseEntity).toDto();
    }

    @Override
    public CourseDTO getCourseById(Long id) {
        Optional<Course> course = courseRepository.findById(id);
        return course.get().toDto();
    }

    @Override
    public CourseDTO updateCourseById(Long id, CourseRequest courseRequest) {
        Course course = courseRepository.findById(id).get();
        course.setCourseName(courseRequest.getCourseName());
        course.setCourseCode(courseRequest.getCourseCode());
        course.setDescription(courseRequest.getDescription());
        course.setInstructor(courseRequest.getInstructor());
        return courseRepository.save(course).toDto();
    }

    @Override
    public void deleteCourseById(Long id) {
        courseRepository.deleteById(id);
    }
}

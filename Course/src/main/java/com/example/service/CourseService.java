package com.example.service;

import com.example.model.DTO.CourseDTO;
import com.example.model.request.CourseRequest;

import java.util.List;

public interface CourseService {
    List<CourseDTO> getAllCourse();

    CourseDTO addCourse(CourseRequest courseRequest);

    CourseDTO getCourseById(Long id);

    CourseDTO updateCourseById(Long id, CourseRequest courseRequest);

    void deleteCourseById(Long id);
}

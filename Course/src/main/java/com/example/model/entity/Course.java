package com.example.model.entity;


import com.example.model.DTO.CourseDTO;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String courseName;
    private String courseCode;
    private String description;
    private String instructor;

    public CourseDTO toDto(){
        return new CourseDTO(this.id, this.courseName, this.courseCode,this.description,this.instructor);
    }

    public Course toEntity(){
        return new Course(null, courseName, courseCode,description,instructor);
    }


}

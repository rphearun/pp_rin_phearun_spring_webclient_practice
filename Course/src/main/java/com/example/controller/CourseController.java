package com.example.controller;

import com.example.model.DTO.CourseDTO;
import com.example.model.request.CourseRequest;
import com.example.model.response.ApiResponse;
import com.example.service.CourseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/course")
public class CourseController {
    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/")
    public ResponseEntity<?> getAllCourse(){
        var course = courseService.getAllCourse();
        ApiResponse<List<CourseDTO>> response = ApiResponse.<List<CourseDTO>>builder()
                .message("successfully fetch data")
                .status("200")
                .payload(course)
                .build();
        return ResponseEntity.ok(response);

    }
    @PostMapping("/add")
    public ResponseEntity<?> addCourse(@RequestBody CourseRequest courseRequest){
        var course = courseService.addCourse(courseRequest);
        ApiResponse<CourseDTO> response = ApiResponse.<CourseDTO>builder()
                .message("successfully fetch data")
                .status("200")
                .payload(course)
                .build();
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getCourseById(@PathVariable Long id){
        CourseDTO course = courseService.getCourseById(id);
        ApiResponse<CourseDTO> response = ApiResponse.<CourseDTO>builder()
                .message("successfully fetch data")
                .status("200")
                .payload(course)
                .build();
        return ResponseEntity.ok(response);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateCourseById(@PathVariable Long id,@RequestBody CourseRequest courseRequest) {
        CourseDTO course = courseService.updateCourseById(id, courseRequest);
        ApiResponse<CourseDTO> response = ApiResponse.<CourseDTO>builder()
                .message("successfully update data")
                .status("200")
                .payload(course)
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCourseById(@PathVariable Long id) {
        courseService.deleteCourseById(id);
        ApiResponse<CourseDTO> response = ApiResponse.<CourseDTO>builder()
                .message("successfully delete data")
                .status("200")
                .build();
        return ResponseEntity.ok(response);
    }


}

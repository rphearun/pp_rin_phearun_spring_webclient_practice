package com.example.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ApiResponse <T> {
    private String message;
    private String status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    T payload;
}
